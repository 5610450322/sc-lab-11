package HashMap_3;
import java.util.HashMap;

public class WordCounter {
	private String message;
	private HashMap<String,Integer> wordCount;
	
	public WordCounter(String message) {
		this.message = message;
	}
	
	public void count(){
		wordCount = new HashMap<String,Integer>();
		String[] strings = message.split(" ");
		int value = 1;
		for (String string : strings) {
			if (wordCount.containsKey(string)){
				value = wordCount.get(string);
				wordCount.remove(string);
				wordCount.put(string, value+1);
				
			}
			else {
				wordCount.put(string, 1);
			}
			
		}
	}
	
	public int hasWord(String word){
		if (wordCount.containsKey(word) == true){
			int value = wordCount.get(word);
			return value;
		}
		return 0;
		
	
		
	}
}
