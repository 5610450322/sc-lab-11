package Exception_4;

public class MyClassMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
			MyClass c = new MyClass();
			System.out.println("A");
			c.mathX();
			System.out.println("B");
			c.mathY();
			System.out.println("C");
			return;
		} catch (DataException e) {
			System.out.println("D");
		} catch (FormatException e){
			System.out.println("E");
		} 
		finally {
			System.out.println("F");
		}
		System.out.println("G");
	}
	
	//1. �ç����͵ mathX() ��ͧ  throw new DataException(); ��� throws DataException ��ͧ declare ���� �� checked exception 
	//   ��ǹ FormatException() ����ͧ declare ������  unchecked exception
	//2. A B C F G
	//3. A D F G
	//4. A B E F G
	//5.public class DataException extends Exception {
	//    public DataException() {}
	//    public DataException(String message) {
	//  	super(message);
	//    }
    //  }
	//6.public class FormatException extends RuntimeException  {
	//  public FormatException() { }
	//    public FormatException(String message) {
	//	     super(message);
	//    }
    //  }

}
