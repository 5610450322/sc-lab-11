package Exception_5;

import java.util.ArrayList;

import Exception_4.DataException;

public class Refrigerator {
	private int size;
	ArrayList<String> things;
	
	public Refrigerator(int size) {
		this.size = size;
	}

	public void put(String stuff) throws FullException{
		things = new ArrayList<String>();
		if (things.size() == size){
			throw new FullException();
		}
		else {
			things.add(stuff);
		}
	}
	
	public String takeOut(String stuff){
		if(things.contains(stuff)){
			return stuff;
		}
		return null;
	}
	
	public String toString(){
		String str = "";		
		for(String i : things){
			str += i + " ";
		}
		return str;
	}
}
	
	
	

