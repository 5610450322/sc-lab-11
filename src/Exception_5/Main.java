package Exception_5;

public class Main {

	public static void main(String[] args){
		// TODO Auto-generated method stub
		Refrigerator rf = new Refrigerator(3);
		try{
		rf.put("fish");
		rf.put("tomato");
		rf.put("butter");
		rf.put("apple");
		rf.put("cake");
		System.out.println(rf.takeOut("milk"));
		}catch (FullException e){
			System.out.println("The refrigerator is Full!");
		}
	
	}

}
